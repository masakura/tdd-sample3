﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using TddSample.Web.Models.Rentals;

namespace TddSample.Web.Models
{
    public sealed class EstimateViewModel
    {
        public EstimateViewModel(IEnumerable<Movie> movies, Customer customer)
        {
            var list = new[]
                {
                    new {Id = "", Value = "選択してください"}
                }.Union(movies.Select(m => new {m.Id, Value = m.Name}))
                .ToImmutableArray();
            Movies = new SelectList(list, "Id", "Value");

            Customer = customer;
        }

        public SelectList Movies { get; }
        public Customer Customer { get; }
        public string SelectedMovieId { get; set; }
        public int DaysRented { get; set; }
    }
}