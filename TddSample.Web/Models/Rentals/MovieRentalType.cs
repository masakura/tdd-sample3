﻿namespace TddSample.Web.Models.Rentals
{
    public enum MovieRentalType
    {
        Regular = 0,
        NewRelease = 1,
        Kids = 2,
    }
}